import React from "react"
// import { Link } from "gatsby"
import { graphql } from "gatsby"
import { connect } from 'react-redux'

import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"

import { toggleDarkMode } from '../state/app'

const IndexPage = ({isDarkMode, dispatch}) => {
  // const products = props.data.allInternalProducts.edges
  return(
    <Layout>
      <SEO title="Home" />

      <h1>Hello</h1>
      <button
        style={isDarkMode ? { background: 'black', color: 'white'} : null}
        onClick={() => dispatch(toggleDarkMode(!isDarkMode))}>
          Dark mode {isDarkMode ? 'on' : 'off'}
        </button>
      {/* {products.map((product, i) => {
        const productData = product.node
        return (
          <div key={productData.index}>
            <p>{productData.productName}</p>
          </div>
        )
      })} */}
      
      {/* <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
        <Image />
      </div>
      <Link to="/page-2/">Go to page 2</Link> */}
    </Layout>
  )
}

export const GatsbyQuery = graphql`
  query Products {
    allInternalProducts {
      edges {
        node {
          id
          index
          productName
          size
          productImage
          price
          isSale
          isExclusive
        }
      }
    }
  }
`

export default connect(state => ({
  isDarkMode: state.app.isDarkMode
}), null)(IndexPage)
